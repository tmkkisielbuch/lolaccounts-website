<?php
require_once('config.php');

?>
<!DOCTYPE html>
<html lang="eng">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title>Shop</title>
<link rel="stylesheet" type="text/css" href="layout.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
</head>
<body>

<header>
    <ul class="container"><center>
    <li class="selected active"><a href="/" style="font-weight: bold; font-size: 20px;"><font color="orange">Lol</font><font color="black">ismurfs</font></a></li>
        <li class="selected active"><a href="/">Home</a></li>
        <li class="unselected"><a href="/shop#actualOffer">Shop</a></li>
        <li class="unselected"><a href="/#aboutUs">About Us</a></li>
        <li class="unselected"><a href="/#whyUs">Why Us</a></li>
        <li class="unselected"><a href="/#faq">FAQ</a></li>
      <li class="unselected"><a href="/contact">Contact</a></li></center>
    </ul>
</header>

<div id="slider" style="background-image: url(img/1.jpg)">
  <div id="sliderText">
    <span>Safe and cheap smurfing</span>

    <span><a href="/shop">Browse our shop now!</a></span>
  </div>
</div>

<div class="obrazekTla" style="background-image: url(img/4.jpg)">

<div style="background-color: rgba(241,244,249,0.9)">
	<div class="container" id="actualOffer" style="padding: 30px 0px;">
		<h1 align="center" >Europe West Unranked LoL Accounts</h1>

		<?php
			$sql = $conn->prepare('SELECT * FROM categories WHERE region = "1"  ORDER BY id DESC ');
			$sql->execute();
			$result = $sql->get_result();

			if ($result->num_rows < 1) {
				echo "<h2>No account available</h2>";
			} else {
				while ($row = $result->fetch_assoc()) {
          $catid = $row['id'];
          $sql = $conn->prepare('SELECT * FROM accounts WHERE categoryID = ? AND isSold = "0"');
          $sql->bind_param('s', $catid);
          $sql->execute();
          $r = $sql->get_result()->num_rows;

					echo '<div class="box">';
          echo '<div class="top">';
          echo '<h2';
          if (strlen($row['title']) > 19 && strlen($row['title']) < 35 ) { echo " style='font-size: 19px'"; } else if (strlen($row['title']) >= 35) { echo " style='font-size: 18px'"; }
          echo '>'.$row['title'].'</h2>';
          echo '</div>';
          echo '<div class="middleText">';
          echo '<h5>'.$row['ipamount'].' IP</h5>';
          echo '<h5>Level 30 / Unranked</h5>
        <h5>Instant Delivery</h5>
        <h5>Lifetime Guarantee</h5>';
          echo '</div>';

          if ($r < 1) {
            echo '<div class="errorNoAcc">No accounts available</div>';
          }  else {
            echo '<div class="errorNoAcc" style="color: green;">Accounts in stock!</div>';
          }
                    
          echo '<div id="buyNowBtn" ';
          if ($r > 0) {
          echo ' onClick=showTos('.$row['id'].')';
          }
          echo '><div class="buynow">Buy Now</div>
        <div class="price">USD '.$row['price'].'</div>
        <div class="clear"></div>
      </div>';
          if ($r > 0) {
            echo '<a href="/payment/purchase?catid='. $row['id'] .'" data-paypal-button="true" id="paypalBtn'.$row['id'].'">';
          }
          if ($r > 0) {
            echo '</a>';
          }
          
          echo '<div class="clear"></div></div>';
				}
			}


		?>

    <h1 align="center" style="margin-top: 50px;">North America Unranked LoL Accounts</h1>

    <?php
      $sql = $conn->prepare('SELECT * FROM categories WHERE region = "2" ORDER BY id DESC ');
      $sql->execute();
      $result = $sql->get_result();

      if ($result->num_rows < 1) {
        echo "<h2>No account available</h2>";
      } else {
        while ($row = $result->fetch_assoc()) {
          $catid = $row['id'];
          $sql = $conn->prepare('SELECT * FROM accounts WHERE categoryID = ? AND isSold = "0"');
          $sql->bind_param('s', $catid);
          $sql->execute();
          $r = $sql->get_result()->num_rows;

          echo '<div class="box">';
          echo '<div class="top">';
          echo '<h2';
          if (strlen($row['title']) > 19 && strlen($row['title']) < 35 ) { echo " style='font-size: 19px'"; } else if (strlen($row['title']) >= 35) { echo " style='font-size: 18px'"; }
          echo '>'.$row['title'].'</h2>';
          echo '</div>';
          echo '<div class="middleText">';
          echo '<h5>'.$row['ipamount'].' IP</h5>';
          echo '<h5>Level 30 / Unranked</h5>
        <h5>Instant Delivery</h5>
        <h5>Lifetime Guarantee</h5>';
          echo '</div>';

          if ($r < 1) {
            echo '<div class="errorNoAcc">No accounts available</div>';
          } else {
            echo '<div class="errorNoAcc" style="color: green;">Accounts in stock!</div>';
          }

          

          
          echo '<div id="buyNowBtn" ';
          if ($r > 0) {
          echo ' onClick=showTos('.$row['id'].')';
          }
          echo '><div class="buynow">Buy Now</div>
        <div class="price">USD '.$row['price'].'</div>
        <div class="clear"></div>
      </div>';
          if ($r > 0) {
            echo '<a href="/payment/purchase?catid='. $row['id'] .'" data-paypal-button="true" id="paypalBtn'.$row['id'].'">';
          }
          if ($r > 0) {
            echo '</a>';
          }
          
          echo '<div class="clear"></div></div>';
        }
      }


    ?>

	</div>
</div>
</div>

<div id="modulo">
  <div id="moduloContent">
  <div id="closeModulo" onClick=closeModulo()>X</div> <h1>Terms of Use</h1>

  - Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
  - Aliquam tincidunt mauris eu risus.
  - Vestibulum auctor dapibus neque.
  - Nunc dignissim risus id metus.
  - Cras ornare tristique elit.
  - Vivamus vestibulum nulla nec ante.
  - Praesent placerat risus quis eros.
  - Fusce pellentesque suscipit nibh.
  - Integer vitae libero ac risus egestas placerat.
  - Vestibulum commodo felis quis tortor.
  - Ut aliquam sollicitudin leo.
  - Cras iaculis ultricies nulla.
  - Donec quis dui at dolor tempor interdum.
  - Vivamus molestie gravida turpis.
  - Fusce lobortis lorem at ipsum semper sagittis.
  - Nam convallis pellentesque nisl.
  - Integer malesuada commodo nulla.
  
  </div>
</div>

<script type="text/javascript">
  function showTos(catid) {
    document.getElementById('modulo').style.display = "block";
    document.getElementById('moduloContent').innerHTML += '<br><center><button id="acceptTos" onClick=AcceptTos('+catid+') style="background-color: #007f00;" >Accept</button><button id="declineTos" onClick=closeModulo() style="background-color: #cc1d00;" >Decline</button></center>';
    document.getElementById('moduloContent').style.display = "block";
    
  }

  function closeModulo() {
      document.getElementById('modulo').style.display = "none";
      document.getElementById('moduloContent').style.display = "none";
      $('button').remove();
      $('br').remove();
    }

  function AcceptTos(catid) {
    document.getElementById('paypalBtn'+catid).click();
  }
</script>


<script type="text/javascript">
  // slider script
  x = 2;
  text = document.getElementById("sliderText2");

  function slideshow() {
    if (x > 3) {
      x = 1;
    }
    document.getElementById("slider").style.backgroundImage="url(img/" + x + ".jpg)";

    x++;
  }
  setInterval(slideshow, 5000);
</script>
</body>
</html>