<?php
require_once('config.php');
?>
<!DOCTYPE>
<html lang="eng">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title>Success</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="layout.css">
</head>

<body>
<header>
    <ul class="container">
    <li class="selected active"><a href="/" style="font-weight: bold; font-size: 20px;"><font color="orange">Lol</font><font color="black">ismurfs</font></a></li>
        <li class="selected active"><a href="/">Home</a></li>
        <li class="unselected"><a href="/shop#actualOffer">Shop</a></li>
        <li class="unselected"><a href="/#aboutUs">About Us</a></li>
        <li class="unselected"><a href="/#whyUs">Why Us</a></li>
        <li class="unselected"><a href="/#faq">FAQ</a></li>
      <li class="unselected"><a href="/contact">Contact</a></li>
    </ul>
</header>

<div id="slider" style="background-image: url(img/1.jpg)">
  <div id="sliderText">
    <span>Safe and cheap smurfing</span>

    <span><a href="/shop">Browse our shop now!</a></span>
  </div>
</div>

<div class="obrazekTla" style="background-image: url(img/4.jpg)">

	<div style="background-color: rgba(241,244,249,0.9)">

		<div class="container" id="thanks" style="padding: 90px 0px;">

			<h1 align="center">Thanks for purchasing account from us!</h1>
			<h2>
			<?php 

			if (isset($_SESSION['aLogin'])) {
				$login = $_SESSION['aLogin'];
			} else {
				$login = "Error";
			}

			if (isset($_SESSION['aPassw'])) {
				$password = $_SESSION['aPassw'];
			} else {
				$password = "Error";
			}

			?><center>
				Account Login: <font color="orange"><?php echo $login; ?></font><br />
				Account Password: <font color="orange"><?php echo $password; ?></font><br />
        </center>
			</h2>

		</div>

	</div>
</div>	



<footer>
  <div class="container">

  </div>
</footer>


<script type="text/javascript">
  // slider script
  x = 2;
  text = document.getElementById("sliderText2");

  function slideshow() {
    if (x > 3) {
      x = 1;
    }
    document.getElementById("slider").style.backgroundImage="url(img/" + x + ".jpg)";

    x++;
  }
  setInterval(slideshow, 5000);
</script>
</body>
</html>