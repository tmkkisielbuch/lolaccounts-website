<?php
require_once('../config.php');

function sendEmail($to, $message, $title) {
	/*
	require ('../PHPMailer/PHPMailerAutoload.php');
	require_once('../config.php');*/
	global $SMTPusername;
	global $SMTPpassword;
	// send mail
	$mail = new PHPMailer;

	$mail->isSMTP();                                      // Set mailer to use SMTP
	$mail->Host = '';                     // Specify main and backup 
	$mail->SMTPAuth = false;                               // Enable SMTP authentication
	$mail->Username = $SMTPusername;   // SMTP username
	$mail->Password = $SMTPpassword;                           // SMTP password
	$mail->SMTPSecure = 'tls';    
	$mail->Port = 587;                        // Enable encryption, only 'tls' is accepted

	$mail->From = ''; // from emait
	$mail->FromName = 'Lol accounts';
	$mail->addAddress($to);                 // Add a recipient

	$mail->WordWrap = 50;                                 // Set word wrap to 50 characters

	$mail->Subject = $title;
	$mail->Body    = $message;

	if(!$mail->send()) {
		return false;
	} else {
	    return true;
	}

}

function randomChars($length) {
	$znaki = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$dlugosc = strlen($znaki);
	$key = "";
	$dlugoscid = $length;
	for ($x = 0; $x <= $dlugoscid; $x++) {
	    $key .= $znaki[rand(0, $dlugosc - 1)];
	}

	return $key;
}

?>