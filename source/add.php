<?php
require_once('config.php');

if (isset($_POST['submitbtn'])) {
  $e = htmlspecialchars($_POST['email']);
  $p = htmlspecialchars($_POST['password']);

  $p = password_hash($p, PASSWORD_DEFAULT);

  $sql = $conn->prepare('INSERT INTO admin (email, password) VALUES (?,?)');
  $sql->bind_param('ss', $e, $p);
  $sql->execute();
}

?>
<!DOCTYPE>
<html lang="eng">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title>Title</title>

</head>

<body>

<form action="" method="POST">
  <input type="email" name="email" placeholder="email">
  <input type="password" name="password" placeholder="password">
  <input type="submit" name="submitbtn" value="Add">
</form>

</body>
</html>