<?php
require_once('../config.php');
require_once('../php/functions.php');

if (isset($_POST['addCatBtn'])) {
	$title = htmlspecialchars($_POST['catTitle']);
	$price = $_POST['catPrice'];
	$ipamount = $_POST['ipamount'];
	$region = $_POST['region'];

	$sql = $conn->prepare('INSERT INTO categories (title, price, ipamount, region) VALUES (?, ?, ?, ?)');
	$sql->bind_param('ssss', $title, $price, $ipamount, $region);
	$sql->execute();
}

if (isset($_POST['editCatBtn'])) {
	$title = htmlspecialchars($_POST['catTitle']);
	$price = $_POST['catPrice'];
	$catid = $_POST['catid'];
	$ipamount = $_POST['ipamount'];
	$region = $_POST['region'];

	$sql = $conn->prepare('UPDATE categories SET title = ?, price = ?, ipamount=?, region = ? WHERE id = ?');
	$sql->bind_param('sssss', $title, $price, $ipamount ,$region,$catid);
	$sql->execute();
	header('Location: /admin/addCategory');
}

if (isset($_POST['delCatBtn'])) {
	$catid = $_POST['catid'];
	$sql = $conn->prepare('DELETE FROM categories WHERE id = ?');
	$sql->bind_param('s', $catid);
	$sql->execute();
	header('Location: /admin/addCategory');
}

?>
<!DOCTYPE>
<html lang="eng">
<head>
<meta charset="UTF-8">

<title>Admin Panel</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="admin.css">
<link rel="stylesheet" type="text/css" href="../layout.css">
</head>

<body>


<?php

if (!isset($_SESSION['adminid'])) { 
	header('Location: /admin/login');
} else {

?>

<div id="leftPanel">
	<a href="/"><div class="przyciskPanelAdmina">Homepage</div></a>
	<a href="/admin"><div class="przyciskPanelAdmina">Dashboard</div></a>
	<a href="accounts"><div class="przyciskPanelAdmina">Manage Accounts</div></a>
	<a href="addAccount"><div class="przyciskPanelAdmina">Add Account</div></a>
	<a href="addCategory"><div class="przyciskPanelAdmina active">Add Category</div></a>
	<a href="messages"><div class="przyciskPanelAdmina">Messages</div></a>
	<a href="logout"><div class="przyciskPanelAdmina">Logout</div></a>
</div>

<div id="rightPanel">
<?php if (!isset($_GET['id'])) { ?>
	<h3>Categories</h3>
	<table>
	<tr class='first'>
		<td width='80%'>Title</td>
		<td width='20%'>Price</td>

	</tr>
	<?php

	$sql = $conn->prepare('SELECT * FROM categories order by id DESC');
	$sql->execute();
	$result = $sql->get_result();
	while ($row = $result->fetch_assoc()) {
		echo "<tr onclick='document.location = \"/admin/addCategory?id=" . $row['id'] ."\";'><td width='50%'>" . $row['title'] ."</td><td width='20%'>" . $row['price'] ."</td>";
		echo "</tr>";
	}

	?>
	</table>

	<div class="clear"></div>

	<h3>Add</h3>
	<form action="" method="POST">
		<label>Title</label>
		<input type="text" name="catTitle" required>
		<label>Price</label>
		<input type="number" name="catPrice" step="0.01" required>
		<label>IP amount</label>
		<input type="number" name="ipamount" required>
		<label>Region</label>
		<select name="region">
			<option value="1">EUW</option>
			<option value="2">NA</option>
		</select>
		<input type="submit" name="addCatBtn" value="Add">
	</form>

	<div class="clear"></div>
<?php } else { ?>

<?php

$catid = $_GET['id'];
$sql = $conn->prepare('SELECT * FROM categories WHERE id = ?');
$sql->bind_param('s', $catid);
$sql->execute();
$result = $sql->get_result()->fetch_assoc();

?>

<h3>Edit</h3>
	<form action="" method="POST">
		<input type="hidden" name="catid" value="<?php echo $result['id']; ?>">
		<label>Title</label>
		<input type="text" name="catTitle" value="<?php echo $result['title']; ?>" required>
		<label>Price</label>
		<input type="number" name="catPrice" value="<?php echo $result['price']; ?>" step="0.01" required>
		<label>IP amount</label>
		<input type="number" name="ipamount" value="<?php echo $result['ipamount']; ?>" step="0.01" required>
		<label>Region</label>
		<select name="region">
			<option value="1" <?php if ($result['region'] == "1") {echo "selected"; }?>>EUW</option>
			<option value="2" <?php if ($result['region'] == "2") {echo "selected"; }?>>NA</option>
		</select>
		<input type="submit" name="editCatBtn" value="Change">
		<input type="submit" name="delCatBtn" value="Delete" style="background-color: red;">
	</form>


<?php } ?>
</div>




<?php

}

?>

</body>
</html>