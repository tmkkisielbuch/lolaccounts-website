<?php
require_once('../config.php');
require_once('../php/functions.php');

?>
<!DOCTYPE>
<html lang="eng">
<head>
<meta charset="UTF-8">

<title>Admin Panel</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="admin.css">
<link rel="stylesheet" type="text/css" href="../layout.css">
</head>

<body>


<?php

if (!isset($_SESSION['adminid'])) { 
	header('Location: /admin/login');
} else {

?>

<div id="leftPanel">
<a href="/"><div class="przyciskPanelAdmina">Homepage</div></a>
	<a href="/admin"><div class="przyciskPanelAdmina">Dashboard</div></a>
	<a href="accounts"><div class="przyciskPanelAdmina active">Manage Accounts</div></a>
	<a href="addAccount"><div class="przyciskPanelAdmina">Add Account</div></a>
	<a href="addCategory"><div class="przyciskPanelAdmina">Add Category</div></a>
	<a href="messages"><div class="przyciskPanelAdmina">Messages</div></a>
	<a href="logout"><div class="przyciskPanelAdmina">Logout</div></a>
</div>

<div id="rightPanel">

<?php

$accID = htmlspecialchars($_GET['id']);

if (isset($_POST['changeBtn'])) {
	$login = htmlspecialchars($_POST['login']);
	$password = htmlspecialchars($_POST['password']);
	$category = htmlspecialchars($_POST['category']);

	$sql = $conn->prepare('UPDATE accounts SET login = ?, password = ?, categoryID = ? WHERE id = ?');
	$sql->bind_param('ssss', $login, $password, $category, $accID);
	$sql->execute();

	echo "<h2>Successfully changed account!</h2>";
}

if (isset($_POST['deleteBtn'])) {
	$sql = $conn->prepare('DELETE FROM accounts WHERE id = ?');
	$sql->bind_param('s', $accID);
	$sql->execute();

	header('Location: /admin/accounts');
}

$sql = $conn->prepare('SELECT * FROM accounts WHERE id = ?');
$sql->bind_param('s', $accID);
$sql->execute();
$result = $sql->get_result();

while ($row = $result->fetch_assoc()) {
	$login = $row['login'];
	$password = $row['password'];
	$category = $row['categoryID'];
}

?>

	<form action="" method="POST">
	<label>Login</label>
	<input type="text" name="login" value="<?php echo $login; ?>" required>
	<label>Password</label>
	<input type="text" name="password" value="<?php echo $password; ?>" required>
	<label>Category</label>
	<select name="category" style="width: 100%; font-size: 16px; padding: 5px;">
	<?php
	$sql = $conn->prepare('SELECT * FROM categories');
	$sql->execute();
	$result = $sql->get_result();

	while ($row = $result->fetch_assoc()) {
		echo '<option value="'.$row['id'].'"';

		if ($row['id'] == $category) {echo " selected ";}

		echo '>'.$row['title'].'</option>';
	}



	?>
	</select>

	<input type="submit" name="changeBtn" value="Change!">

	<input type="submit" style="background-color: #f3654d" name="deleteBtn" value="DELETE ACCOUNT">
	</form>

	<div class="clear"></div>
</div>




<?php

}

?>

</body>
</html>