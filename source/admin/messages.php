<?php
require_once('../config.php');
require_once('../php/functions.php');

?>
<!DOCTYPE>
<html lang="eng">
<head>
<meta charset="UTF-8">

<title>Admin Panel</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="admin.css">
<link rel="stylesheet" type="text/css" href="../layout.css">
</head>

<body>


<?php

if (!isset($_SESSION['adminid'])) { 
	header('Location: /admin/login');
} else {

?>

<div id="leftPanel">
	<a href="/"><div class="przyciskPanelAdmina">Homepage</div></a>
	<a href="/admin"><div class="przyciskPanelAdmina">Dashboard</div></a>
	<a href="accounts"><div class="przyciskPanelAdmina">Manage Accounts</div></a>
	<a href="addAccount"><div class="przyciskPanelAdmina">Add Account</div></a>
	<a href="addCategory"><div class="przyciskPanelAdmina">Add Category</div></a>
	<a href="messages"><div class="przyciskPanelAdmina active">Messages</div></a>
	<a href="logout"><div class="przyciskPanelAdmina">Logout</div></a>
</div>

<?php
	if (!isset($_GET['id'])) { 
?>

<div id="rightPanel">
	<h3>New messages</h3>
	<table>
	<tr class='first'>
		<td width='15%'>First Name</td>
		<td width='15%'>Last Name</td>
		<td width='25%'>Email</td>
		<td width='45%'>Message</td>
	</tr>
	<?php

	$sql = $conn->prepare('SELECT * FROM messages WHERE isRead = "0" order by id DESC');
	$sql->execute();
	$result = $sql->get_result();
	while ($row = $result->fetch_assoc()) {
		if (strlen($row['message']) > 50) {
			$message = substr($row['message'], 0, 47) . " ...";
		} else {
			$message = substr($row['message'], 0, 50);
		}
		echo "<tr onclick='document.location = \"/admin/messages?id=" . $row['id'] ."\"'>
		<td width='15%'>" . $row['fName'] . "</td>
		<td width='15%'>" . $row['lName'] . "</td>
		<td width='25%'>" . $row['email'] . "</td>
		<td width='45%'>" . $message . "</td>
		</tr>";
	}

	?>
	</table>

	<h3>All messages</h3>
	<table>
	<tr class='first'>
		<td width='15%'>First Name</td>
		<td width='15%'>Last Name</td>
		<td width='25%'>Email</td>
		<td width='45%'>Message</td>
	</tr>
	<?php

	$sql = $conn->prepare('SELECT * FROM messages order by id DESC LIMIT 50');
	$sql->execute();
	$result = $sql->get_result();
	while ($row = $result->fetch_assoc()) {
		if (strlen($row['message']) > 50) {
			$message = substr($row['message'], 0, 47) . " ...";
		} else {
			$message = substr($row['message'], 0, 50);
		}
		echo "<tr onclick='document.location = \"/admin/messages?id=" . $row['id'] ."\"'>
		<td width='15%'>" . $row['fName'] . "</td>
		<td width='15%'>" . $row['lName'] . "</td>
		<td width='25%'>" . $row['email'] . "</td>
		<td width='45%'>" . $message . "</td>
		</tr>";
	}

	?>
	</table>

	<div class="clear"></div>
</div>




<?php
} else { ?>

<?php

$mID = $_GET['id'];

$sql = $conn->prepare('SELECT * FROM messages WHERE id = ?');
$sql->bind_param('s', $mID);
$sql->execute();
$result = $sql->get_result();
while ($row = $result->fetch_assoc()) {

	$fName = $row['fName'];
	$lName = $row['lName'];
	$email = $row['email'];
	$message = $row['message'];

}

$sql = $conn->prepare('UPDATE messages SET isRead = "1" WHERE id = ?');
$sql->bind_param('s', $mID);
$sql->execute();

echo "";
?>
<div id="rightPanel">

<style type="text/css">
	.container div {
		margin-top: 10px;
	}
</style>

<div class="container">
	<div>From: <b><?php echo $fName . " " . $lName; ?></b></div>
	<div>Email: <b><?php echo $email; ?></b></div>
	<div>Message:<br /> <b><?php echo $message; ?></b></div>
</div>

	<div class="clear"></div>
</div>

<?php
}
}

?>

</body>
</html>