<?php
require_once('../config.php');
require_once('../php/functions.php');

?>
<!DOCTYPE>
<html lang="eng">
<head>
<meta charset="UTF-8">

<title>Admin Panel</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="admin.css">
<link rel="stylesheet" type="text/css" href="../layout.css">
</head>

<body>


<?php

if (!isset($_SESSION['adminid'])) { 
	header('Location: /admin/login');
} else {

?>

<div id="leftPanel">
	<a href="/"><div class="przyciskPanelAdmina">Homepage</div></a>
	<a href="/admin"><div class="przyciskPanelAdmina">Dashboard</div></a>
	<a href="accounts"><div class="przyciskPanelAdmina active">Manage Accounts</div></a>
	<a href="addAccount"><div class="przyciskPanelAdmina">Add Account</div></a>
	<a href="addCategory"><div class="przyciskPanelAdmina">Add Category</div></a>
	<a href="messages"><div class="przyciskPanelAdmina">Messages</div></a>
	<a href="logout"><div class="przyciskPanelAdmina">Logout</div></a>
</div>

<div id="rightPanel">
	<h3>Accounts</h3>
	<table>
	<tr class='first'>
		<td width='20%'>Login</td>
		<td width='20%'>Category</td>
		<td width='20%'>Price</td>
		<td width='20%'>Is Sold</td>
	</tr>
	<?php

	$sql = $conn->prepare('SELECT accounts.id, accounts.login, accounts.password, accounts.isSold, categories.price, categories.title FROM accounts, categories WHERE accounts.categoryID=categories.id order by accounts.id DESC');
	$sql->execute();
	$result = $sql->get_result();
	while ($row = $result->fetch_assoc()) {
		echo "<tr onclick='document.location = \"/admin/editAccount?id=" . $row['id'] ."\";'><td width='20%'>" . $row['login'] ."</td><td width='20%'>" . $row['title'] ."</td><td width='20%'>" . $row['price'] ."$</td>";
		if ($row['isSold'] == "0") {
			echo "<td width='20%'>No</td>";
		} else {
			echo "<td width='20%'>Yes</td>";
		}
		echo "</tr>";
	}

	?>
	</table>

	<div class="clear"></div>
</div>




<?php

}

?>

</body>
</html>