<?php
require_once('../config.php');
require_once('../php/functions.php');

?>
<!DOCTYPE>
<html lang="eng">
<head>
<meta charset="UTF-8">

<title>Admin Panel</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="admin.css">
<link rel="stylesheet" type="text/css" href="../layout.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>


</head>

<body>


<?php

if (isset($_POST['loginBtn'])) {
	$email = htmlspecialchars($_POST['mail']);
	$pass = htmlspecialchars($_POST['password']);


	$sql = $conn->prepare('SELECT * FROM admin WHERE email = ?');
	$sql->bind_param('s', $email);
	$sql->execute();
	$result = $sql->get_result();

	if ($result->num_rows < 1) {
		echo "<h1>Wrong email or password</h1>";
	} else {
		while ($row = $result->fetch_assoc()) {
			$p = $row['password'];
			$uid = $row['id'];
		}
		
		if (password_verify($pass, $p)) {
			$_SESSION['adminid'] = $uid;
			header('Location: /admin');
		} else {
			echo "<h1>Wrong email or password 2</h1>";
		}
	}
}

if (isset($_POST['forgotBtn'])) {
	$code = randomChars(20);
	$email = htmlspecialchars($_POST['mail']);

	$sql = $conn->prepare('SELECT * FROM admin WHERE email = ?');
	$sql->bind_param('s', $email);
	$sql->execute();

	$result = $sql->get_result();

	if ($result->num_rows < 1) {
		echo "<h1>No user with that email</h1>";
	} else {
		while ($row = $result->fetch_assoc()) {
			$uid = $row['id'];
		}
		$sql = $conn->prepare('INSERT INTO resetpass (userID, code) VALUES (?, ?)');
		$sql->bind_param('ss', $uid, $code);
		$sql->execute();

		$message = "Your reset link: " . "http://" .$_SERVER['SERVER_NAME'] . '/admin/login?r=' . $code;
		$to = $email;
		$title = "Reset Password";
		if (sendEmail($to, $message, $title)) {
			echo "Email with reset code has been sent";
		} else {
			echo "Error while sending email";
		}
	}
}

if (isset($_POST['resetBtn'])) {
	$nPass = htmlspecialchars($_POST['nPass']);
	$code = $_POST['code'];

	$password = password_hash($nPass, PASSWORD_DEFAULT);

	$sql = $conn->prepare('SELECT * FROM resetpass WHERE code = ?');
	$sql->bind_param('s', $code);
	$sql->execute();

	$result = $sql->get_result();

	if ($result->num_rows < 1) {
		echo "<h1>Error</h1>";
	} else {
		while ($row = $result->fetch_assoc()) {
			$uid = $row['userID'];
		}
		$sql = $conn->prepare('UPDATE resetpass SET used = "1" WHERE code = ?');
		$sql->bind_param('s', $code);
		$sql->execute();

		$sql = $conn->prepare('UPDATE admin SET password = ? WHERE id = ?');
		$sql->bind_param('ss', $password, $uid);
		$sql->execute();

		echo "Password changed successfuly, you can now login";
	}
}




?>

<?php

if (!isset($_SESSION['adminid'])) { 
	if (isset($_GET['forgot'])) { ?>
	<form id="loginForm" action="" method="POST">
		<div class="formularzowyNaglowek">Account Email Address:</div>
		<input type="email" name="mail" placeholder="Email address" required>
		<center><input type="submit" name="forgotBtn" value="Reset"></center>
	</form> 

<?php } else if (isset($_GET['r'])) { ?>

	<form id="loginForm" action="" method="POST">
		<div class="formularzowyNaglowek">New Password:</div>
		<input type="password" name="nPass" placeholder="New password" required>
		<input type="hidden" name="code" value="<?php echo $_GET['r'] ?>" required>
		<center><input type="submit" name="resetBtn" value="Reset"></center>
	</form> 

<?php
	} else { ?>
		<h2 style="text-align: center; margin-top: 30px;">Login to admin panel</h2>
		<form id="loginForm" action="" method="POST">
		<div class="formularzowyNaglowek">Email Address:</div>
		<input type="email" name="mail" placeholder="Email address" required>
		<div class="formularzowyNaglowek">Password:</div>
		<input type="password" name="password" placeholder="Password" required>
		<center><input type="submit" name="loginBtn" value="Login"></center>
		<a href="/admin/login?forgot">Forgot your password?</a>
		<div class="clear"></div>
		</form> 
		
<?php
	}
} else {
	header('Location: /admin');
}





?>

</body>
</html>