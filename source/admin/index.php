<?php
require_once('../config.php');
require_once('../php/functions.php');

?>
<!DOCTYPE>
<html lang="eng">
<head>
<meta charset="UTF-8">

<title>Admin Panel</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="admin.css">
<link rel="stylesheet" type="text/css" href="../layout.css">
</head>

<body>


<?php

if (!isset($_SESSION['adminid'])) { 
	header('Location: /admin/login');
} else {

?>

<div id="leftPanel">
<a href="/"><div class="przyciskPanelAdmina">Homepage</div></a>
	<a href="/admin"><div class="przyciskPanelAdmina active">Dashboard</div></a>
	<a href="accounts"><div class="przyciskPanelAdmina">Manage Accounts</div></a>
	<a href="addAccount"><div class="przyciskPanelAdmina">Add Account</div></a>
	<a href="addCategory"><div class="przyciskPanelAdmina">Add Category</div></a>
	<a href="messages"><div class="przyciskPanelAdmina">Messages</div></a>
	<a href="logout"><div class="przyciskPanelAdmina">Logout</div></a>
</div>

<div id="rightPanel">
	<h3>Recent payments</h3>
	<table>
	<tr class='first'>
		<td width='20%'>Account Login</td>
		<td width='20%'>Account Password</td>
		<td width='20%'>Date</td>
		<td width='20%'>Amount</td>
		<td width='20%'>Payment ID</td>
	</tr>
	<?php

	$sql = $conn->prepare('SELECT accounts.login AS Login, accounts.password AS Pass, date, amount, paymentID FROM payments INNER JOIN accounts ON payments.accountId=accounts.id order by date DESC');
	$sql->execute();
	$result = $sql->get_result();
	while ($row = $result->fetch_assoc()) {
		echo "<tr><td width='20%'>" . $row['Login'] ."</td><td width='20%'>" . $row['Pass'] ."</td><td width='20%'>" . $row['date'] ."</td><td width='20%'>" . $row['amount'] ."$</td><td width='20%'>" . $row['paymentID'] ."</td></tr>";
	}

	?>
	</table>

	<div class="clear"></div>
</div>




<?php

}

?>

</body>
</html>