<?php
require_once('../config.php');
require_once('../php/functions.php');

?>
<!DOCTYPE>
<html lang="eng">
<head>
<meta charset="UTF-8">

<title>Admin Panel</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="admin.css">
<link rel="stylesheet" type="text/css" href="../layout.css">
</head>

<body>


<?php

if (!isset($_SESSION['adminid'])) { 
	header('Location: /admin/login');
} else {



?>

<div id="leftPanel">
<a href="/"><div class="przyciskPanelAdmina">Homepage</div></a>
	<a href="/admin"><div class="przyciskPanelAdmina">Dashboard</div></a>
	<a href="accounts"><div class="przyciskPanelAdmina">Manage Accounts</div></a>
	<a href="addAccount"><div class="przyciskPanelAdmina active">Add Account</div></a>
	<a href="addCategory"><div class="przyciskPanelAdmina">Add Category</div></a>
	<a href="messages"><div class="przyciskPanelAdmina">Messages</div></a>
	<a href="logout"><div class="przyciskPanelAdmina">Logout</div></a>
</div>

<div id="rightPanel">

<?php

	if (isset($_POST['addMore']) || isset($_POST['addOne'])) {

		$login = htmlspecialchars($_POST['login']);
		$password = htmlspecialchars($_POST['password']);
		$category = htmlspecialchars($_POST['category']);

		$sql = $conn->prepare('INSERT INTO accounts (login, password, categoryID) VALUES (?, ?, ?)');
		$sql->bind_param('sss', $login, $password, $category);
		$sql->execute();

		if (isset($_POST['addMore'])) {
			echo "<h2>Successfully added account!</h2>";
		} else {
			header('Location: /admin/accounts');
		}

	}


?>
	
	<form action="" method="POST">

	<label>Login</label>
	<input type="text" name="login" placeholder="Login" required>
	<label>Password</label>
	<input type="text" name="password" placeholder="Password" required>
	<label>Category</label>
	<select name="category" style="width: 100%; font-size: 16px; padding: 5px;">
	<?php
	$sql = $conn->prepare('SELECT * FROM categories');
	$sql->execute();
	$result = $sql->get_result();

	while ($row = $result->fetch_assoc()) {
		echo '<option value="'.$row['id'].'">'.$row['title'];
		if ($row['region'] == 1) {
			echo ' - EUW';
		} else if ($row['region'] == 2) {
			echo ' - NA';
		}
		echo '</option>';
	}



	?>
	</select>

	<input type="submit" name="addMore" value="Confirm, and add more">
	<input type="submit" name="addOne" value="Confirm, and finish">


	</form>

	<div class="clear"></div>
</div>




<?php

}

?>

</body>
</html>