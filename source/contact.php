<?php
require_once('config.php');
?>
<!DOCTYPE html>
<html lang="eng">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title>Contact</title>
<link rel="stylesheet" type="text/css" href="layout.css">
</head>
<body>
<style type="text/css">
</style>

<header>
    <ul class="container"><center>
		<li class="selected active"><a href="/" style="font-weight: bold; font-size: 20px;"><font color="orange">Lol</font><font color="black">ismurfs</font></a></li>
  			<li class="selected active"><a href="/">Home</a></li>
  			<li class="unselected"><a href="/shop#actualOffer">Shop</a></li>
  			<li class="unselected"><a href="#aboutUs">About Us</a></li>
  			<li class="unselected"><a href="#whyUs">Why Us</a></li>
  			<li class="unselected"><a href="#faq">FAQ</a></li>
			<li class="unselected"><a href="/contact">Contact</a></li></center>
		</ul>
</header>

<div id="slider" style="background-image: url(img/1.jpg)">
  <div id="sliderText">
    <span>Safe and cheap smurfing</span>

    <span><a href="/shop">Browse our shop now!</a></span>
  </div>
</div>

<div class="obrazekTla" style="background-image: url(img/5.jpg)">

  <div style="background-color: rgba(241,244,249,0.9)">

  
    <div id="contactDiv" class="container" style="padding: 30px 0px;">

    <?php

  if (isset($_POST['submitBtn'])) {
    $fName = htmlspecialchars($_POST['fName']);
    $lName = htmlspecialchars($_POST['lName']);
    $email = htmlspecialchars($_POST['email']);
    $message = htmlspecialchars($_POST['message']);

    $sql = $conn->prepare('INSERT INTO messages (message, email, fName, lName) VALUES (?, ?, ?, ?)');
    $sql->bind_param('ssss', $message, $email, $fName, $lName);
    

    if ($sql->execute()) {
      echo '<div id="successDiv">Your message has been sent successfully. We will contact you as fast as possible</div>';
    } else {
      echo '<div id="successDiv">Error, please try again</div>';
    }

    
  }

  ?>

      <h1 style="text-align: center;">Contact Us!</h1>

      <p>Skype: <a href="skype:xrobinhd?add">Skype</a></p>
      <p>Mail: contact@mail.com</p>

      <form action="" method="POST">

        <label>First Name:</label>
        <input type="text" name="fName" maxlength="50" required />

        <label>Last Name:</label>
        <input type="text" name="lName" maxlength="50" required />

        <label>Email Address:</label>
        <input type="mail" name="email" maxlength="100" required />

        <label>Comment:</label>
        <textarea name="message" maxlength="1000" required ></textarea>

        <center><input type="submit" name="submitBtn" value="Send"></center>

      </form>

    </div>

  </div>

</div>



<footer>
  <div class="container">

  </div>
</footer>

<script type="text/javascript">
  // slider script
  x = 2;
  text = document.getElementById("sliderText2");

  function slideshow() {
    if (x > 3) {
      x = 1;
    }
    document.getElementById("slider").style.backgroundImage="url(img/" + x + ".jpg)";

    x++;
  }
  setInterval(slideshow, 5000);
</script>

</body>
</html>

