<?php
require_once('../config.php');
require __DIR__  . '/PayPal-PHP-SDK/autoload.php';
require 'bootstrap.php';
	use PayPal\Api\Amount;
	use PayPal\Api\Details;
	use PayPal\Api\Item;
	use PayPal\Api\ItemList;
	use PayPal\Api\Payer;
	use PayPal\Api\Payment;
	use PayPal\Api\RedirectUrls;
	use PayPal\Api\Transaction;


if (isset($_GET['catid'])) {

		$accountID = $_GET['catid'];

		$sql = $conn->prepare('SELECT * FROM accounts WHERE categoryID = ? AND isSold="0" LIMIT 1');
		$sql->bind_param('s', $accountID);
		$sql->execute();
		$result = $sql->get_result();

		if ($result->num_rows < 1) {
			header('Location: /shop');
			die();
		} else {
			while ($row = $result->fetch_assoc()) {
				$accid = $row['id'];
			}

			$sql = $conn->prepare('SELECT * FROM categories WHERE id = ?');
			$sql->bind_param('s', $accountID);
			$sql->execute();
			$result = $sql->get_result();
			while ($row = $result->fetch_assoc()) {
				$price = $row['price'];
			}
		}

		$payer = new Payer();
		$payer->setPaymentMethod("paypal");

		$item1 = new Item();
		$item1->setName('LOL account')
		    ->setCurrency('USD')
		    ->setQuantity(1)
		    ->setPrice((float)$price);

		$amount = new Amount();
		$amount->setCurrency("USD")
		    ->setTotal((float)$price);

		$transaction = new Transaction();
		$transaction->setAmount($amount)
	    ->setDescription(" Price: " . $price . "$")
	    ->setInvoiceNumber(uniqid());

	    $cancelurl = "http://" .$_SERVER['SERVER_NAME'] . '/';
	    $baseUrl = getBaseUrl();
		$redirectUrls = new RedirectUrls();
		$redirectUrls->setReturnUrl("$baseUrl/executepayment?acc=" . $accid ."&success=true")
		    ->setCancelUrl($cancelurl);


		$payment = new Payment();
		$payment->setIntent("sale")
		    ->setPayer($payer)
		    ->setRedirectUrls($redirectUrls)
		    ->setTransactions(array($transaction));

		try {
		    $payment->create($apiContext);
		    $paymentID = $payment->getId();
		    $approvalUrl = $payment->getApprovalLink();

		    $_SESSION['paymentID'] = $paymentID;

		    header('Location: '.$approvalUrl);
		} catch (Exception $ex) {
			echo "Error";
		}

}
