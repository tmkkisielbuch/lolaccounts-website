<?php
require_once('../config.php');
require_once('../php/functions.php');
require __DIR__ . '/bootstrap.php';
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\ExecutePayment;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;



if (isset($_GET['success']) && $_GET['success'] == 'true') {

	if (isset($_GET['acc'])) {
		$paymentId = $_GET['paymentId'];
	    $payment = Payment::get($paymentId, $apiContext);


	    if ($paymentId == $_SESSION['paymentID']) {
	    	$accountID = $_GET['acc'];

		   

	    	$sql = $conn->prepare('SELECT * FROM accounts WHERE id = ?');
			$sql->bind_param('s', $accountID);
			$sql->execute();
			$result = $sql->get_result();

			while ($row = $result->fetch_assoc()) {
				$login = $row['login'];
				$pass = $row['password'];
				$categoryid = $row['categoryID'];
			}

			$sql = $conn->prepare('SELECT * FROM categories WHERE id = ?');
			$sql->bind_param('s', $categoryid);
			$sql->execute();
			$result = $sql->get_result();
			while ($row = $result->fetch_assoc()) {
				$price = $row['price'];
			}

	    	$execution = new PaymentExecution();
		    $execution->setPayerId($_GET['PayerID']);

		    $result = $payment->execute($execution, $apiContext);

		    try {
		        $payment = Payment::get($paymentId, $apiContext);
		    } catch (Exception $ex) {
		    	echo "error";
		        exit(1);
		    }
		    
		    

			$sql = $conn->prepare('INSERT INTO payments (amount, accountId, paymentID) VALUES (?, ?, ?)');
			$sql->bind_param('sss', $price, $accountID, $paymentId);
			$sql->execute();

			$sql = $conn->prepare('UPDATE accounts SET isSold = "1", sellDate = NOW() WHERE id = ?');
			$sql->bind_param('s', $accountID);
			$sql->execute();

			$_SESSION['aLogin'] = $login;
			$_SESSION['aPassw'] = $pass;

			header('Location: /success#thanks');

		}
	}

}
?>