## About
Lolaccounts website is shop project where you can buy accounts to League of Legends. Payments are done via Paypal SDK. This project was created for one of my client, unfortunately (or fortunately, because now I can share code here) his domain expired, and project is not available online. 

## Installation
To get this project working you will need:
- sql server
- webserver with php 7 support
- PHP SDK for Paypal into /source/payment folder (https://github.com/paypal/PayPal-PHP-SDK)

- import lolaccounts3.sql into your database
- add admin panel on /add.php page

To accept payments you will need 
- PHP SDK for Paypal into /source/payment folder (https://github.com/paypal/PayPal-PHP-SDK)
- add client id and client secret key in /source/payment/bootstrap.php file

```php
$clientId = '';
$clientSecret = '';
```

## Design


<p align="center">
  <img src="https://i.imgur.com/cbqye3b.png" width="400"/>
  <img src="https://i.imgur.com/jIyKvBX.png" width="400"/>
  <img src="https://i.imgur.com/udEfj5z.png" width="400"/>
  <img src="https://i.imgur.com/qKqn2dN.png" width="400"/>
</p>

## Contact
You can contact me via:
- Skype: Smirnoffq
- Mail: bartlomiej.gorkiewicz@gmail.com 